# Generated by Django 3.0 on 2022-03-31 06:51

from django.db import migrations, models
import django.db.models.deletion
import mdeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20220220_1402'),
        ('auth', '0011_update_proxy_permissions'),
        ('wares', '0005_auto_20220323_2303'),
        ('sales', '0002_salesdetails_discount'),
    ]

    operations = [
        migrations.CreateModel(
            name='Promotion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='促销计划单号（CharField）', max_length=60, verbose_name='促销计划单号')),
                ('discount', models.DecimalField(blank=True, decimal_places=2, default=1, help_text='折扣（CharField）', max_digits=10, null=True, verbose_name='折扣')),
                ('desc', mdeditor.fields.MDTextField(blank=True, verbose_name='说明')),
                ('grade', models.SmallIntegerField(default=0, verbose_name='生效优先级')),
                ('repeat', models.BooleanField(default=False, help_text='是否可叠加（BooleanField）', verbose_name='是否可叠加')),
                ('entry_date', models.DateTimeField(blank=True, help_text='生效时间（DateTimeField）', null=True, verbose_name='生效时间')),
                ('stop_date', models.DateTimeField(blank=True, help_text='结束时间（DateTimeField）', null=True, verbose_name='结束时间')),
                ('created_date', models.DateTimeField(auto_now_add=True, help_text='交易时间（DateTimeField）', null=True, verbose_name='交易时间')),
                ('last_edited_date', models.DateTimeField(auto_now=True, help_text='最后编辑时间（DateTimeField）', null=True, verbose_name='最后编辑时间')),
                ('deleted', models.CharField(choices=[('0', '未删除'), ('1', '已删除')], default='0', help_text='是否删除（CharField，可选值：0，1）', max_length=1, verbose_name='是否删除')),
                ('created_by', models.ForeignKey(blank=True, help_text='创建人ID（ForeignKey）', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Promotion_cb', to='users.UserExtension', verbose_name='创建人ID')),
                ('group', models.ManyToManyField(blank=True, help_text='可见分组', related_name='Promotion_group', to='auth.Group', verbose_name='可见分组')),
                ('last_edited_by', models.ForeignKey(blank=True, help_text='最后编辑人ID（ForeignKey）', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Promotion_eb', to='users.UserExtension', verbose_name='最后编辑人ID')),
                ('waresku', models.ManyToManyField(blank=True, help_text='商品SKU（ForeignKey）', related_name='Promotion_waresku', to='wares.WareSKU', verbose_name='商品SKU')),
            ],
            options={
                'verbose_name': '促销计划',
                'verbose_name_plural': '促销计划',
                'ordering': ['grade', 'id'],
            },
        ),
    ]
